export interface IProducts {
    id: number;
    name: string;
    description: string;
    image: string;
    categoryId: number;
    price: number;
}
