export class Category {
    constructor(
        public readonly id: number,
        public name: string,
    ) {

    }
}