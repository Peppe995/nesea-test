import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { Product } from '../interfaces/models/product.model';
import { IProducts } from '../interfaces/products.interfaces';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(private _http: HttpClient) {

  }
  public list(id: string): Observable<Product[]> {
    let url = `${environment.url}/products`; {

      if (id) {
        url += `?category_id=${id}`;
      }

      return this._http.get<IProducts[]>(url).pipe(
        map(e => {
          const product: Product[] = [];
          for (const p of e) {
            product.push(new Product(p.id, p.name, p.description, p.image, p.price, p.categoryId));
          }
          return product;
        })
      );
    }
  }
}
