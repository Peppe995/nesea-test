import { Injectable } from '@angular/core';
import { Product } from '../interfaces/models/product.model';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  public prodottoCarello: Product[];
  constructor() {
    this.prodottoCarello = [];
  }
  get counter() {
    return this.prodottoCarello.length;
  }

  public addCart(p: Product) {
    return this.prodottoCarello.push(p);
  }
  public removeCart(p: Product) {
    return this.prodottoCarello.splice(p);
  }
  public list() {
    return this.prodottoCarello;
  }
}
