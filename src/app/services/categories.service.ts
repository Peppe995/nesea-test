import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { Product } from '../interfaces/models/product.model';
import { IProducts } from '../interfaces/products.interfaces';
import { Observable } from 'rxjs';
import { ICategories } from '../interfaces/categories.intefaces';
import { Category } from '../interfaces/models/category.model';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {

  constructor(private _http: HttpClient) {

  }
  public navCategory() {
    return this._http.get<ICategories[]>(`${environment.url}/categories`).pipe(
      map(e => {
        const categories: Category[] = [];

        for (const c of e) {
          categories.push(new Category(c.id, c.name));
        }
        return categories;
      })
    );
  }
}
