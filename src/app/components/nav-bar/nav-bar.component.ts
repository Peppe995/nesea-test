import { Component, OnInit } from '@angular/core';
import { CategoriesService } from 'src/app/services/categories.service';
import { Category } from 'src/app/interfaces/models/category.model';
import { ActivatedRoute } from '@angular/router';
import { CartService } from 'src/app/services/cart.service';


@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {
  public categories: Category[];

  constructor(private _navbarService: CategoriesService, public _cartCounter: CartService) {
    this.categories = [];
  }


  ngOnInit() {
    this._navbarService.navCategory().subscribe(categories => this.categories = categories);
  }

  get counter() {
    return this._cartCounter.counter;
  }
}
