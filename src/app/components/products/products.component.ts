import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductsService } from 'src/app/services/products.service';
import { Product } from 'src/app/interfaces/models/product.model';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  public products: Product[]
  constructor(private _route: ActivatedRoute, private _sP: ProductsService) {
    this.products = [];

  }

  ngOnInit() {
    this._route.paramMap.subscribe(params => {
      this._sP.list(params.get('id')).subscribe(p => this.products = p);
    });
  }

}
