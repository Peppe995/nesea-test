import { Component, OnInit, Input } from '@angular/core';
import { Product } from 'src/app/interfaces/models/product.model';
import { CartService } from 'src/app/services/cart.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
  @Input() public product: Product[];
  constructor(public _listaCarello: CartService) {
    this.product = [];
  }

  ngOnInit() {
    this.product = this._listaCarello.prodottoCarello;
  }


}
