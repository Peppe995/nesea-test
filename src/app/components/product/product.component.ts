import { Component, OnInit, Input, Output } from '@angular/core';
import { Product } from 'src/app/interfaces/models/product.model';
import { CartService } from 'src/app/services/cart.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
  @Input() public product: Product;
  @Output() public p: Product;

  constructor(public cartService: CartService) { }

  ngOnInit() {
  }
  public add(p: Product) {
    this.cartService.addCart(p);
  }
  public remove(p: Product) {
    this.cartService.removeCart(p);
  }

}
