import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductsComponent } from './components/products/products.component';
import { CartComponent } from './components/cart/cart.component';

const routes: Routes = [{
  component: ProductsComponent,
  path: 'prodotti'
}, {
  component: ProductsComponent,
  path: 'prodotti/:id'
}, {
  component: CartComponent,
  path: 'cart'
}, {
  redirectTo: 'prodotti',
  path: '',
  pathMatch: 'full'
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
